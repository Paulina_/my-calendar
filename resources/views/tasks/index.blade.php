@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <table class="table table-condensed table-hover">
            <thead>
                 <tr>
                     <th>Type</th>
                     <th>Task</th>
                     <th>Location</th>
                     <th>Date and time</th>
                     <th>Done</th>
                     <th></th>
                     <th></th>
                 </tr>
            </thead>
        @forelse($tasks as $task)
             <tbody>
                 <tr>
                    @if($task->type == 0)
                        <td>{{ 'Meeting' }}</td>
                    @elseif(($task->type == 1))
                        <td>{{ 'Call' }}</td>
                    @elseif(($task->type == 2))
                        <td>{{ 'Conference' }}</td>
                    @elseif(($task->type == 3))
                        <td>{{ 'Matter' }}</td>
                    @endif
                     
                    <!-- <td>{{ $task->type }} </td> -->
                    <td>{{ str_limit($task->task, 100) }}</td>
                    <td>{{ $task->location}}</td>
                    <td>{{ $task->date}}</td>
                    
                    <td> 
                        <input type="checkbox" name="done" class="form-check-input" @if($task->is_completed == 1) checked @endif>
                        
                    </td>
                    @if(auth()->id() === $task->user->id)
                        <td>

                            <a href="{{ route('tasks.edit', $task) }}" class="btn btn-warning btn-sm">
                                <i class="fa fa-pencil"></i> {{('Edit') }}
                            </a>
                            <a href="{{ route('tasks.destroy', $task) }}" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i> {{('Delete') }}
                            </a>

<!--                             <a href="{{ route('tasks.show', $task) }}" class="btn btn-primary btn-sm">
                                <i class="fa fa-eye"></i> {{('Show') }}
                            </a> -->


<!--                              <a href="javascript:;" class="btn btn-danger btn-sm" onclick="document.getElementById('task-{{$task->id}}').submit();">
                                <i class="fa fa-trash"></i> Delete
                            <form id="task-{{$task->id}}" action="{{ route('tasks.destroy', $task) }}" method="POST" style="display: none;">
                            @method('DELETE')
                            @csrf 
                    </form>
                    </a>
                        </td> -->
                    @endif
                    <td>
                        
                    </td>
                 </tr>
             </tbody>

<!--      
                
                <div class="card-footer">
                    <a href="{{ route('tasks.show', $task) }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-eye"></i> {{('Show') }}
                    </a>
                    @if(auth()->id() === $task->user->id)
                    <a href="{{ route('tasks.edit', $task) }}" class="btn btn-warning btn-sm">
                        <i class="fa fa-pencil"></i> {{('Edit') }}
                    </a>
                    <a href="javascript:;" class="btn btn-danger btn-sm" onclick="document.getElementById('task-{{$task->id}}').submit();">
                        <i class="fa fa-trash"></i> {{('Delete') }}
                    </a>
                    <form id="task-{{$task->id}}" action="{{ route('tasks.destroy', $task) }}" method="POST" style="display: none;">
                        @method('DELETE')
                        @csrf
                    </form>
                    @endif
                </div>
            </div>
        </div> -->
        @empty
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">{{('You have no tasks yet :(') }}</div> <br>
                <div class="card-body">
                    <a href="{{ route('tasks.create') }}" class="btn btn-primary">{{('Create A New Task') }}</a>
                </div>
            </div>
        </div>
        @endforelse
        <div class="card-fotter">
            <a href="{{ route('tasks.create') }}" class="btn btn-primary">{{('Create A New Task') }}</a>
            <a href="{{ route('tasks.create') }}" class="btn btn-primary">{{('Current Tasks') }}</a>
            <br><br>
        </div>
        {{--  @forelse($tasks as $task)
        Task id: {{ $task->id }} <br>
        Task: {{ $task->task }} <br>
        @empty
        You have no tasks yet :(
        @endforelse  --}}
    </div>
</div>
@endsection