@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <label>Create A New Task</label>
                </div>

                <div class="card-body">

                    <form action="{{ route('tasks.store') }}" method="POST">
                        <!-- @csrf -->
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Theme:</label>
                            <input type="text" name="task" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Type:</label>
                            <br>
                            <select name='type'>
                                <option value = '0'>Meeting</option>
                                <option value = '1'>Call</option>
                                <option value = '2'>Conference</option>
                                <option value = '3'>Matter</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Duration:</label>
                            <br>
                            <select name='duration'>
                                <option value = '0'>10 minutes</option>
                                <option value = '1'>15 minutes</option>
                                <option value = '2'>45 minutes</option>
                                <option value = '3'>1 hour</option>
                                <option value = '4'>2 hours</option>
                                <option value = '5'>3 hours</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Date and time:</label>
                                <div class="row">
                                    <div class='col-sm-6'>
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="text" class="timepicker form-control" name="datetime"" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <script type="text/javascript">
                                        $('.timepicker').datetimepicker({
                                            format: 'YY/MM/DD HH/mm/ss'
                                            // YYYYMMDDHHMMSS
                                            // format: 'YY/MM/DD'
                                        }); 
                                    </script>
                        </div>
                        

                        <div class="form-group">
                            <label>Location:</label>
                            <input type="text" name="location" class="form-control">                          
                        </div>

                        <div class="form-group">
                            <label>Comment:</label>
                            <input type="text" name="comment" class="form-control">                          
                        </div>


                        <br>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>

                        <!-- <span class="pull-right"> -->
                        <a href="{{ route('tasks.index') }}" class="btn btn-primary">
                            <i class="fa fa-undo"></i></i> Back
                        </a>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection