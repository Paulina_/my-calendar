@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ ('Edit Task') }} 
                    #  
                    {{ "$task->task" or 'Default'}}
                </div>
                <div class="card-body">
                    <form action="{{ route('tasks.update', $task) }}" method="POST">
                        {{ csrf_field() }}


                        <div class="form-group">
                            <label>Theme:</label>
                            <input type="text" name="task" class="form-control" value={{ "$task->task" or 'Default'}}>
                        </div>

                        <div class="form-group">
                            <label>Type:</label>
                            <br>
                            <select name='type'>
                                <option value = '0' <?= ("$task->type" == 'meeting') ? selected : ''?> >Meeting</option>
                                <option value = '1' <?= ("$task->type" == 'call') ? selected : ''?> >Call</option>
                                <option value = '2' <?= ("$task->type" == 'conference') ? selected : ''?> >Conference</option>
                                <option value = '3' <?= ("$task->type" == 'matter') ? selected : ''?> >Matter</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Duration:</label>
                            <br>
                            <select name='duration'>
                                <option value = '0'>10 minutes</option>
                                <option value = '1'>15 minutes</option>
                                <option value = '2'>45 minutes</option>
                                <option value = '3'>1 hour</option>
                                <option value = '4'>2 hours</option>
                                <option value = '5'>3 hours</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Date and time:</label>
                                <div class="row">
                                    <div class='col-sm-6'>
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="text" name="datetime" class="timepicker form-control" value={{ "$task->datetime" or 'Default'}} />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <script type="text/javascript">
                                        $('.timepicker').datetimepicker({
                                            format: 'YY/MM/DD HH/mm/ss'
                                        }); 
                                    </script>
                        </div>
                        

                        <div class="form-group">
                            <label>Location:</label>
                            <input type="text" name="location" class="form-control" value={{ "$task->location" or 'Default'}} >                          
                        </div>

                        <div class="form-group">
                            <label>Comment:</label>
                            <input type="text" name="comment" class="form-control" value={{ "$task->comment" or 'Default'}}>                          
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>

                        <!-- <span class="pull-right"> -->
                        <a href="{{ route('tasks.index') }}" class="btn btn-primary">
                            <i class="fa fa-undo"></i></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection