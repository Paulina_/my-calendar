<!-- @extends('layouts.app') -->

<!-- @section('content') -->
    <!-- <div class="text-center"> -->
        <!-- <h1>Welcome to My Calendar!</h1> -->
        <!-- <hr/> -->

        <!-- <h3>Log in or register</h3> -->

    <!-- </div> -->

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('tasks.index') }}" class="btn btn-primary">Go to Tasks</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection