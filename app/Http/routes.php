<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::get('/', function () {
    return view('/layouts/app');
});

/* *****************************************************************
Route::auth();
// Route::get('/home', 'HomeController@index');
// Route::get('/task/list', 'TaskController@index');
Route::resource('task', 'TaskController', ['middleware' => 'auth']);
// Route::get('/home', 'HomeController@index')->name('home');
****************************************************************** */

// Auth::routes();

Route::auth();

Route::group(['middleware' => 'auth'], function () {
    Route::resource('tasks', 'TaskController');
    Route::get('/tasks', 'TaskController@index')->name('tasks.index');
    Route::get('/tasks/create', 'TaskController@create')->name('tasks.create');
    Route::post('/tasks', 'TaskController@store')->name('tasks.store');

    Route::get('/tasks/{task}/edit', 'TaskController@edit')->name('tasks.edit');
    Route::get('/tasks/{task}/show', 'TaskController@show')->name('tasks.show');

    Route::patch('/tasks/{task}', 'TaskController@update')->name('tasks.update');

    Route::get('/tasks/{task}/destroy', 'TaskController@destroy')->name('tasks.destroy');

    Route::get('/home', 'HomeController@index')->name('home');
});
